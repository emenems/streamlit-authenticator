import string
import random
import re

def generate_random_pw(length: int=16) -> str:
    """
    Generates a random password.

    Parameters
    ----------
    length: int
        The length of the returned password.
    Returns
    -------
    str
        The randomly generated password.
    """
    letters = string.ascii_letters + string.digits
    return ''.join(random.choice(letters) for i in range(length)).replace(' ','')

def check_pw_requirement(password) -> bool:
    """Check if password complies with DT rules
    contains lowercase, uppercase, number, and special ASCII character,
    and must be min 12 character long
    
    Parameters:
    -----------
        password (str). Input password

    Returns:
    --------
        bool True if complies, False if not
    
    Example:
    --------
        >>> assert(check_password("1234567891011")) is False
        >>> assert(check_password("123456789aB!")) is True
        >>> assert(check_password("123456789aBB")) is False
        >>> assert(check_password("123456789aB")) is False
        >>> assert(check_password("abcdefghijklm")) is False
        >>> assert(check_password("abcdefghijklM")) is False
        >>> assert(check_password("abcdefghijklM@")) is False
        >>> assert(check_password("abcdefghijk1M@")) is True
    """
    if len(password) < 12:
        return False
    if not re.search("[a-z]", password):
        return False
    if not re.search("[A-Z]", password):
        return False
    if not re.search("[0-9]", password):
        return False
    if not re.search(r"[!@#?()\\\/_\-+*^%;:|<>~]", password):
        return False
    return True
